(in-package :lrp)

(defun make-floor ()
  (loop for i from 0 to 9 do
       (loop for j from 0 to 9 do
	    (setf (aref *map* i j 2) (make-instance 'tile :material-key :floor)))))

(defun tick ()
  (loop for ent in *entities* do
       (if (typep ent 'physical)
	   (update-location ent))))

(defun run-game (player)
  (let (key)
    (loop while (not (eql key #\q)) do
	 (tick)
	 (setf key (read-char))
	 (cond
	   ((eql key #\w) (push-entity player (vec 0 1 0)))
	   ((eql key #\s) (push-entity player (vec 0 -1 0)))
	   ((eql key #\a) (push-entity player (vec -1 0 0)))
	   ((eql key #\d) (push-entity player (vec 1 1 0))))
	 (show-layer 2))))

(defun main ()
  (setf *entities* ())
  (make-floor)
  (setf (aref *map* 4 4 2) (make-instance 'tile :material-key :wall))
  (setf player (make-instance 'creature))
  (setf (location player) (vec 3 1 2))
  (print (render-layer *map* 2))
  (run-game player))
