(in-package :lrp)

(defclass tile (entity)
  ((material-key :accessor material-key :initarg :material-key :initform :air :type symbol)))

(defmethod material ((tile tile))
  (material (material-key tile)))
