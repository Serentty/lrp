(in-package :lrp)

(defun clear-screen ()
  (format t "~C[2J" #\Esc))

(defun render-layer (map z)
  (let ((layer-image ()) (size-x (array-dimension map 0)) (size-y (array-dimension map 1)))
    ;; Draw tiles
    (loop for y from 0 upto (1- size-y) do
	 (loop for x from (1- size-x) downto 0 do
	      (push (let ((tile-mat (material-key (aref map x y z))))
		      (cond
			((equal tile-mat :floor) "O")
			((equal tile-mat :wall)  "#")
			(t "-")))
		    layer-image)
	      (loop for ent in *entities* do
		   (if (typep ent 'physical)
		       (let* ((loc (location ent))
			      (loc-x (round (vref loc 0))) (loc-y (round (vref loc 1))) (loc-z (round (vref loc 2))))
			 (if (and (= loc-x x) (= loc-y y) (= loc-z z))
        		     (progn
			       (pop layer-image)
			       (push (draw-entity ent) layer-image)))))))
	 (push #\linefeed layer-image))
    ;; Draw entities
    (format nil "~{~a~^ ~}" layer-image)))

(defun show-layer (z)
  (clear-screen)
  (print (render-layer *map* z)))
