(in-package :lrp)

(defgeneric push-entity (ent direction-vector))
(defgeneric update-location (ent))

(defmethod push-entity ((ent physical) direction-vector)
  (setf (velocity ent) (v+ (velocity ent) direction-vector)))

(defmethod update-location ((ent physical))
  (setf (location ent) (v+ (location ent) (velocity ent))))
