(in-package :lrp)

(defclass entity ()
  ((id :accessor id :initform (uuid:make-v1-uuid) :type uuid:uuid)))

(defclass physical (entity)
  ((location :accessor location :initform (vec 0 0 0) :type vec)
   (velocity :accessor velocity :initform (vec 0 0 0) :type vec)))

(defvar *entities* ())

(defmethod initialize-instance :after ((ent entity) &key)
  (push ent *entities*))
