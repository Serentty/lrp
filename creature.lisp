(in-package :lrp)

(defclass creature (physical)
  ((hp :accessor hp :initform 100 :type 'integer)))
