(defsystem :lrp
  :description "Lisp Roguelike Project"
  :version "0.0.0"
  :depends-on ("gamebox-math" "uuid")
  :serial t
  :components ((:file "package")
	       (:file "entity")
	       (:file "motion")
	       (:file "creature")
       	       (:file "material")
	       (:file "tile")
	       (:file "map")
	       (:file "drawing")
	       (:file "renderer")
	       (:file "game")))
